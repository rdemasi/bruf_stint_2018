PRISM
=====

Version: 4.4
Date: Wed Aug 22 18:11:59 ART 2018
Hostname: Nando
Memory limits: cudd=1g, java(heap)=910.5m
Command line: prism /home/nando/development/bruf_stint_2018/examples/Case3/traffic0-3/model.prism -pctl 'Pmax=?[F node=3]' -exportadv /home/nando/development/bruf_stint_2018/examples/Case3/traffic0-3/adv.tra -exportlabels /home/nando/development/bruf_stint_2018/examples/Case3/traffic0-3/label.txt -exportstates /home/nando/development/bruf_stint_2018/examples/Case3/traffic0-3/states.txt -s

Parsing model file "/home/nando/development/bruf_stint_2018/examples/Case3/traffic0-3/model.prism"...

1 property:
(1) Pmax=? [ F node=3 ]

Type:        MDP
Modules:     bundle 
Variables:   node ts n0 n1 n2 n3 

Building model...

Computing reachable states...

Reachability (BFS): 8 iterations in 0.00 seconds (average 0.000000, setup 0.00)

Time for model construction: 0.021 seconds.

Warning: Deadlocks detected and fixed in 4 states

Type:        MDP
States:      22 (1 initial)
Transitions: 32
Choices:     27

Transition matrix: 230 nodes (7 terminal), 32 minterms, vars: 9r/9c/6nd

Exporting list of reachable states in plain text format to file "/home/nando/development/bruf_stint_2018/examples/Case3/traffic0-3/states.txt"...

Exporting labels and satisfying states in plain text format to file "/home/nando/development/bruf_stint_2018/examples/Case3/traffic0-3/label.txt"...

Warning: Disabling Prob1 since this is needed for adversary generation

---------------------------------------------------------------------

Model checking: Pmax=? [ F node=3 ]

Warning: Disabling Prob1 since this is needed for adversary generation

Prob0A: 6 iterations in 0.00 seconds (average 0.000000, setup 0.00)

yes = 4, no = 7, maybe = 11

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=22, nc=16, nnz=21, k=2] [0.3 KB]
Building action information... [0.1 KB]
Creating vector for yes... [0.2 KB]
Allocating iteration vectors... [2 x 0.2 KB]
Allocating adversary vector... [0.1 KB]
TOTAL: [1.0 KB]

Starting iterations...

Iterative method: 7 iterations in 0.00 seconds (average 0.000000, setup 0.00)

Adversary written to file "/home/nando/development/bruf_stint_2018/examples/Case3/traffic0-3/adv.tra".

Value in the initial state: 0.81

Time for model checking: 0.005 seconds.

Result: 0.81 (value in the initial state)

---------------------------------------------------------------------

Note: There were 3 warnings during computation.

