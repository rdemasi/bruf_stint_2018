# Simple network with 3 contacts

NUM_OF_NODES = 4
CONTACTS = [{'from': 0 ,'to': 1, 'ts': 0, 'pf':0.5},{'from': 0 ,'to': 2, 'ts': 1, 'pf':0.1},{'from': 1 ,'to': 2, 'ts': 2, 'pf':0.1}, {'from': 2 ,'to': 3, 'ts': 3, 'pf':0.2}, {'from': 1 ,'to': 3, 'ts': 4, 'pf':0.1} ]
TRAFFIC = {'from': 0, 'to': 3, 'ts': 0}
NUM_OF_TS = max([c['ts'] for c in CONTACTS]) + 1



