from random import seed, random, choice
import sys
'''
Given a num_of_nodes, num_of_ts and a density(in [0,1])
it builds a network where in each ts nodes has probability "density"
of being conected (link are directed).

returns a dict{'num_of_nodes':int, 'contacts':list[{'from':int,'to':int, 'ts':int}]}
ESTO SOLO DEBERIA GENERAR REDES CON 1 CONTACT X NODO
'''
def gen_random_network(num_of_nodes, num_of_ts, density, seed_=0):
    seed(seed_)
    net = {'NUM_OF_NODES':num_of_nodes, 'CONTACTS':[]}
    for ts in range(num_of_ts):
        nodes = [x for x in range(num_of_nodes)]
        while len(nodes)>1:
            n1 = choice(nodes)
            nodes.remove(n1)
            n2 = choice(nodes)
            nodes.remove(n2)
            if random()<= density:
                net['CONTACTS'].append({'from':n1,'to':n2, 'ts':ts})
    return net


'''
Given a net:dict{'num_of_nodes':int,'num_of_ts':int, 'contacts':list[{'from':int,'to':int, 'ts':int}]},
a ts_duration (to compute from and to for any contact) and a capacity:
    It returns the corresponding DTNSim Contact Plan as string.

'''
def get_DTNSimCP_fromNet(net, ts_duration, capacity):
    cp = ""
    for c in net['CONTACTS']:
        cp += "a contact +%d +%d %d %d %d\n"%(c['ts']*ts_duration,(c['ts']+1)*ts_duration,c['from'] + 1,c['to'] + 1, capacity)

    return cp
'''
Given a
    net: {'NUM_OF_NODES':int, 'CONTACTS':[{'from':int,'to':int,'ts':int}]}
    output_path: string
    This method prins a file at output_path with a dot representation of current network.
'''
def net_to_dot(net, output_path, file_name='net.dot'):
    dot_str = "digraph G { \n\n"
    dot_str += "rank=same;\n"
    dot_str += "ranksep=equally;\n"
    dot_str += "nodesep=equally;\n"

    NUM_OF_TS = max([c['ts'] for c in net['CONTACTS']]) + 1
    for ts in range(NUM_OF_TS):
        dot_str += "\n// TS = %d\n"%ts
        for n in range(net["NUM_OF_NODES"]):
            dot_str +=  "%d.%d[label=L%d];\n"%(n,ts,n);

        dot_str += str(net["NUM_OF_NODES"]) + "." + str(ts) + "[shape=box,fontsize=16,label=\"TS " + str(ts) + "\"];\n"

        for n in range(net["NUM_OF_NODES"]):
            dot_str += "%d.%d -> %d.%d[style=\"invis\"];\n"%(n,ts,n+1,ts)

        for c in net['CONTACTS']:
            if c['ts'] == ts:
                dot_str += "%d.%d -> %d.%d%s\n"%(c['from'],ts, c['to'],ts, "[color=green,fontcolor=green,penwidth=2]"  if "FLAG" in c.keys() else "")

    dot_str += "\n\n// Ranks\n"
    for n in range(net["NUM_OF_NODES"]):
        dot_str+="{ rank = same;"
        for ts in range(NUM_OF_TS):
            dot_str += " %d.%d;"%(n,ts)
        dot_str += "}\n"
    dot_str += " \n}"

    output_path = output_path + (lambda s: "/" if s[-1]!='/' else '')(output_path) + file_name
    f = open(output_path, 'w')
    f.write(dot_str)
    f.close()


'''
Given a
    net: {'NUM_OF_NODES':int, 'CONTACTS':[{'from':int,'to':int,'ts':int}]}
    output_path: string
    This method prints a file at output_path/file_name with a python representation of current network.
'''
def net_to_file(net, output_path, file_name='net.py'):
    f = open(output_path + (lambda s: "/" if s[-1]!='/' else '')(output_path) + file_name, 'w')
    s = "NUM_OF_NODES = %d\n" % (net['NUM_OF_NODES'])
    s += "CONTACTS = %s \n" % (net["CONTACTS"])
    if 'TRAFFIC' in net.keys():
        #Traffic is optional
        s += "TRAFFIC = %s\n" % (net['TRAFFIC'])

    f.write(s)
    f.close()


'''
Given a
    net: {'NUM_OF_NODES':int, 'CONTACTS':[{'from':int,'to':int,'ts':int}]}
    If return the contact {'from':f,'to':t,'ts':ts} if exists. Otherwise
    it return none.
'''
def get_contact_from_net(net,f,t,ts):
    for c in net['CONTACTS']:
        if c['from'] == f and c['to'] == t and c['ts'] == ts:
            return c

'''
Given a
    net: {'NUM_OF_NODES':int, 'CONTACTS':[{'from':int,'to':int,'ts':int}]}
    If return an integer which is the index of the contact in contact list if it exists. Otherwise
    it return none.
'''
def get_contact_index_from_net(net,f,t,ts):
    for i in range(len(net['CONTACTS'])):
        c = net['CONTACTS'][i]
        if c['from'] == f and c['to'] == t and c['ts'] == ts:
            return i


'''
Given a
    net: {'NUM_OF_NODES':int, 'CONTACTS':[{'from':int,'to':int,'ts':int}]}
    routing_decision: {'action':string, ('send': routing_decision) ('fail': routing_decision)}
modify net contacts adding a field 'FLAG'=1 in those that will be used according the given routing decisions
'''
def highlight_routing_decisions(net, routing_decision):
    routing = [routing_decision]
    while len(routing) > 0:
        current = routing.pop(0)
        action = current['action'][6:]
        from_ = int(action[:action.index('_')])
        to = int(action[action.index('_') + 2:action.rindex('_')])
        ts = int(action[action.rindex('_') + 1:])
        if "send" in current.keys():
            routing.append(current['send'])

        if "fail" in current.keys():
            routing.append(current['fail'])

        get_contact_from_net(net, from_, to, ts)['FLAG'] = 1

'''
Given
    net: {'NUM_OF_NODES':int, 'CONTACTS':[{'from':int,'to':int,'ts':int}]}
    routing_decision: {'action':string, ('send': routing_decision) ('fail': routing_decision)}
    target: int, the targeted node for wich was computed the best routing
    ts_duration: The duration assigned to a time stamp

return a [{'from':int,'to':int,'expire_time':ts*int,'contact_id':int}] which encodes the routing decisions
'''
def gen_routing_for_DTNSim(net, routing_decisions, source, target, ts_duration):
    contacts = []
    routing = [routing_decisions]
    while len(routing) > 0:
        current = routing.pop(0)
        action = current['action'][6:]
        from_ = int(action[:action.index('_')])
        to = int(action[action.index('_') + 2:action.rindex('_')])
        ts = int(action[action.rindex('_') + 1:])
        id = get_contact_index_from_net(net,from_,to,ts)
        #contacts.append({'from':from_,'to':target,'expire_time':ts*ts_duration,'contact_id':id})
        #change expire_time: a contact is validad until the end of its time_stamp
        contacts.append({'source': source, 'target':target, 'c_from': from_,'expire_time':(ts + 1) * ts_duration, 'contact_id':id, 'c_to':to})
        if "send" in current.keys():
            routing.append(current['send'])

        if "fail" in current.keys():
            routing.append(current['fail'])

    #If there is a contact from a to b at ts, and from b to a at  ts delete both because a and b has the same delivery probability
    new_contacts = []
    for c in contacts:
        # Si yo soy el receptor de algun otro que es receptor mio en el mismo ts borro ambos contactos
        if len(list(filter(lambda x: x['c_from']==c['c_to'] and x['c_to']==c['c_from'] and x['expire_time']==c['expire_time'],contacts))) == 0:
            #Add this routing decisions if it has not been already added (multiple path can give the same routing decision)
            if len(list(filter(lambda x: x['c_from']==c['c_from'] and x['c_to']==c['c_to'] and x['expire_time']==c['expire_time'] and x['contact_id']==c['contact_id'],new_contacts))) == 0:
                new_contacts.append(c)
            else:
                print("Routing decision %s had been already added." % (str(c)))
        else:
            print("Routing decision %s was deleted, because it is redundant."%(str(c)))




    return new_contacts

'''
Check is a contact is valid, returns true or false
'''
def contact_is_valid(c, num_of_nodes,pf_is_required=True):
    if type(c) == dict and all(k in c.keys() for k in ['from', 'to', 'ts']) and \
        type(c['from']) == int and \
        type(c['to']) == int and \
        type(c['ts']) == int and \
        0 <= c['from'] < num_of_nodes and \
        0 <= c['to'] < num_of_nodes and \
        c['from'] != c['to'] and \
        c['ts'] >= 0 and \
        (not pf_is_required or
        ('pf' in  c.keys()  and type(c['pf']) == float and 0. <= c['pf'] <= 1.)):
                return True
    return False


def validate_traffic(t, num_of_nodes):
	if type(t) == dict and all(k in t.keys() for k in ['from', 'to', 'ts']) and \
	type(t['from']) == int and type(t['to']) == int and type(t['ts']) == int and \
	0 <= t['from'] < num_of_nodes and 0 <= t['to'] < num_of_nodes and \
	t['from'] != t['to'] and t['ts'] >= 0:
		return True
	else: 
		return False
	


'''
Get Net from file, if traffic required it returns an error if it does not exist.
returns
    {'NUM_OF_NODES':int, 'CONTACTS':[{'from':int,'to':int,'ts':int}] (,'TRAFFIC':{'from':int,'to':int, 'ts':int})}
'''
def get_net_from_file(path_to_net, traffic_required=False, contact_pf_required=True):
    input = {}
    f = exec(open(path_to_net,'r').read(),input)

    #CHECK INPUT FILE HAS THE REQUIRED FIELDS
    if 'NUM_OF_NODES' in input.keys():
        if type(input['NUM_OF_NODES']) != int or input['NUM_OF_NODES'] <= 1:
            print("[ERROR] NUM_OF_NODES must be an integer greater than 1")
            return {}
        else:
            NUM_OF_NODES = input['NUM_OF_NODES']
    else:
        print("[ERROR] The input network must contain NUM_OF_NODES")
        return {}

    if 'CONTACTS' in input.keys():
        if type(input['CONTACTS']) != list or len(input['CONTACTS']) < 1:
            print("[ERROR] CONTACTS must be a list with at least 1 element")
            return {}
        else:
            # Check if each contact is write in the correct way
            for c in input['CONTACTS']:
                if not contact_is_valid(c, NUM_OF_NODES, contact_pf_required):
                    print("[ERROR] Contact must described for a dict: {'from':int,'to':int,'ts':int (,'pf':float)} where: ")
                    print("\t to, from are different and to,from in [0,NUM_OF_NODES)")
                    print("\t ts >= 0")
                    print("\t pf in [0.,1.] (pay attention to write the dots!)")
                    print("\t %s does not satisfy the above properties." % str(c))
                    return {}

            CONTACTS = input['CONTACTS']
    else:
        print("[ERROR] The input network must contain CONTACTS:[{'from':int,'to':int,'ts':int, 'pf':float}]")
        return {}

    # Traffic is readed if it exists. If traffic_required, it reports an error when traffic does not exist
    if 'TRAFFIC' in input.keys():
        t = input['TRAFFIC']        
        if type(t)==dict and validate_traffic(t,NUM_OF_NODES):
            return {'NUM_OF_NODES': NUM_OF_NODES, 'CONTACTS': CONTACTS, 'TRAFFIC':[t]}
        elif type(t)==list and all(validate_traffic(tt,NUM_OF_NODES) for tt in t):
            return {'NUM_OF_NODES': NUM_OF_NODES, 'CONTACTS': CONTACTS, 'TRAFFIC':t}
        else:
            print("[ERROR] TRAFFIC must be a list of dicts: [{'from':int,'to':int,'ts':int}] where: ")
            print("\t to, from are different and to,from in [0,NUM_OF_NODES)")
            print("\t ts >= 0")
            return {}
	
    elif traffic_required:
        print("[ERROR] The input network must contain TRAFFIC:{'from':int,'to':int,'ts':int}")
        return {}
    # End Check

    return {'NUM_OF_NODES':NUM_OF_NODES, 'CONTACTS':CONTACTS}


def get_net_from_dtnsim_CP(cp_path, ts_duration):
    contacts=[]
    with open(cp_path) as fin:
        for line in fin:
            if 'a contact' in line:
                line = line.split(' ')
                ts_from = int(line[2][1:]) / ts_duration
                ts_to = int(line[3][1:]) / ts_duration
                if ts_to - ts_from != 1:
                    print("[ERROR] auxiliaryfunctions-get_net_from_dtnsim_CP: It can not translate networks which has contact with more than 1 ts duration. " +\
                          "ts_duration was setted to %d"%ts_duration)
                    exit()
                node_from = int(line[4]) - 1
                node_to = int(line[5]) - 1
                contacts.append({'from':node_from,'to':node_to,'ts':int(ts_from)})

    return {'NUM_OF_NODES': max([c['from'] for c in contacts] + [c['to'] for c in contacts]) + 1, 'CONTACTS':contacts}
