'''
Best Routing Under Uncertainties

This file implements a method to build a prism model with computes the best
routing decisions for a 1 traffic under uncertainties.

Use: 
	 1) python3 BRUF-1T input_file : Output files will be stored in the input_file directory
	 2) python3 BRUF-1T input_file output_directory


'''

#!/usr/bin/python3
import os
import sys
from auxiliaryfunctions import get_net_from_file, highlight_routing_decisions, net_to_dot, gen_routing_for_DTNSim
from model import get_best_routing


####################################################################################
################################### MAIN PROGRAM ###################################
####################################################################################

#Parse command line arguments
if len(sys.argv) == 2:
    INPUT_FILE = sys.argv[1]
    # It choose the output folder as the folder where the input file is stored
    if '/' in INPUT_FILE:
        OUTPUT_DIR = INPUT_FILE[0:INPUT_FILE.rindex('/') + 1]
    else:
        OUTPUT_DIR = ''
elif len(sys.argv) == 3:
    INPUT_FILE =  sys.argv[1]
    OUTPUT_DIR = sys.argv[2]
    if OUTPUT_DIR[-1] != '/':
        OUTPUT_DIR+='/'
else:
    print("Use: \n\t 1) BRUF-1T input_file : Output files will be stored in the input_file directory")
    print("\t 2) BRUF-1T input_file output_directory")
    exit()


print("[Info] Parsing input file: %s"%INPUT_FILE)
net = get_net_from_file(INPUT_FILE,traffic_required=True, contact_pf_required=True)
print("[Info] Computing best routing: %s"%INPUT_FILE)

try:
    os.remove(OUTPUT_DIR + '/routing-dtnsim.txt')
except OSError:
    pass

for t in net['TRAFFIC']:
	print("[Info] Computing best routing from node %d to node %d"%(t['from'], t['to']))
	traffic_dir = OUTPUT_DIR + "traffic%d-%d"%(t['from'], t['to'])
	if not os.path.exists(traffic_dir):
		os.mkdir(traffic_dir)
	best_routing = get_best_routing(net, traffic_dir, traffic=t)
	if best_routing[1] != {}:
		  highlight_routing_decisions(net, best_routing[1])
		  net_to_dot(net, traffic_dir)
			
			#Print routing decisions for DTNSim
		  f = open(OUTPUT_DIR + '/routing-dtnsim.txt', 'a')
		  f.write("# From %d to %d\n" % (t['from'], t['to']))
		  for c in gen_routing_for_DTNSim(net, best_routing[1], t['from'], t['to'], 10):
		      f.write("%d %d %d %d %d \n" % (c["c_from"] + 1, c["source"] + 1,c["target"] + 1, c["expire_time"], c["contact_id"] + 1))
		  f.write("\n\n")
		  f.close()
	else:
		  net_to_dot(net, traffic_dir)
		  print("[Info] There is not route from %d to %d" % (n1, n2))

print("[Done]")

