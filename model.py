import os
from parseadversary import parse_adversary_file, get_routing_decisions
from setting import PATH_TO_PRISM


def print_str_to_file(data, output_path):
    f = open(output_path, 'w')
    f.write(data)
    f.close()

def get_empty_visited_nodes_form(num_of_nodes):
    return (" & ").join(["(n%d'=false)"%(n) for n in range(num_of_nodes)])


def get_model_from_net(net, traffic=None):
    if traffic == None:
        traffic = net["TRAFFIC"]

    res = "mdp\n\n"
    res += "const int NUM_OF_NODES = %d;\n"%(net['NUM_OF_NODES'])

    NUM_OF_TS = max([c["ts"] for c in net["CONTACTS"]]) + 1
    res += "const int NUM_OF_TS = %d;\n\n"%(NUM_OF_TS)

    res += "module bundle \n"
    res += "\t node: [0..NUM_OF_NODES - 1] init %d;\n"%(traffic['from'])
    res += "\t ts: [0..NUM_OF_TS] init 0;\n\n"

    res += "\t //To take account visited nodes in current ts\n"
    for n in range(net['NUM_OF_NODES']):
        res += "\t n%d: bool init false;\n"%(n)

    res += "\n"

    for c in net["CONTACTS"]:
        if (c["from"] != traffic["to"]): #Ignore contacts outgoing from target node
            # res += "\t [send_n%d_n%d_%d] ts=%d & node=%d  -> "%(c['from'],c['to'],c['ts'],c['ts'],c['from'])
            # res += "%f: (ts'=ts+1) + "%(c['pf']) # Failure case
            # res += "1 - %f: (node'=%d);\n"%(c['pf'], c['to']) # send case

            res += "\t [send_n%d_n%d_%d] ts=%d & node=%d & !n%d -> "%(c['from'],c['to'],c['ts'],c['ts'],c['from'], c['to'])
            res += "%f: (ts'=ts+1) & %s + "%(c['pf'],get_empty_visited_nodes_form(net["NUM_OF_NODES"])) # Failure case
            res += "1 - %f: (node'=%d) & (n%d'=true);\n"%(c['pf'], c['to'], c['from']) # send case

    #res += "\t [next] ts < NUM_OF_TS -> (ts'= ts + 1);\n"
    res += "\t [next] ts < NUM_OF_TS -> (ts'= ts + 1) & %s;\n"%(get_empty_visited_nodes_form(net["NUM_OF_NODES"]))
    res += "endmodule\n"
    return res


'''
It Computes the Best Routing for a given network and returns it as a string to be used as input of
    DTNSim BRUF1T routing
'''
def get_best_routing(net, working_dir,traffic=None):
    if traffic == None:
        if 'TRAFFIC' in net.keys():
            traffic = net['TRAFFIC']
        else:
            print("[Error] BRUF-1T:get_best_routing - traffic argument shouldn't be None or traffic['TRAFFIC'] must be defined.")
            exit()

    NUM_OF_TS =  max([c['ts'] for c in net['CONTACTS']]) + 1

    model = get_model_from_net(net,traffic=traffic)
    path_to_model = working_dir + "/model.prism"
    print_str_to_file(model, path_to_model)
    prop = "Pmax=?[F node=%d]"%traffic['to']
    os.system("%s %s -pctl '%s' -exportadv %s/adv.tra -exportlabels %s/label.txt -exportstates %s/states.txt -s > %s" % (PATH_TO_PRISM, path_to_model, prop, working_dir, working_dir,working_dir, working_dir + '/prism-output.txt'))


    #Debug
    root = parse_adversary_file(working_dir + "/adv.tra",working_dir + "/label.txt" )
    print_str_to_file(str(root), working_dir + "/adv-parsed.json")

    f = open(working_dir + '/prism-output.txt', 'r')
    lines = f.readlines()
    line = next(filter(lambda l: l.startswith('Result:'), lines))
    line = list(filter(lambda w: w != '', line.split(' ')))
    line = list(filter(lambda w: w != '', line))
    if line[0] != 'Result:':
        print("[Error] it expected 'Result' but found %s" % (line[0]))
        exit()
    pmax = float(line[1])
    if pmax > 0:
        routing_decision = get_routing_decisions(working_dir + "/adv.tra", working_dir + "/states.txt", working_dir + "/label.txt")
        if routing_decision != {}:
            print_str_to_file(str(routing_decision), working_dir + "/bestRouting.json")
        else:
            print("[Error] WorkingDir:%s :pmax>0 but routing decisions is empty. "%(working_dir))

        print("PMAX from %d to %d: %f" % (traffic['from'],traffic['to'], pmax))
        return (pmax,routing_decision)
    else:
        return (0.,{}) #No solution was found

